import * as actionTypes from '../actions/actionTypes';

const initialState={
	ingredients:null,
    totalPrice: 20,
    error:false,
    building:false

};
const INGREDIENTS_PRICES = {
    salad: 10,
    cheese: 30,
    meat: 40,
    bacon: 40


}



const reducer=(state=initialState,action)=>{
	switch(action.type){
		case actionTypes.ADD_INGREDIENT:
		return{
			...state,
			ingredients:{
				...state.ingredients,
				[action.ingredientName]:state.ingredients[action.ingredientName] + 1
			},
			totalPrice:state.totalPrice+ INGREDIENTS_PRICES[action.ingredientName],
			building:true

		};
		case actionTypes.REMOVE_INGREDIENT:
		return {
			...state,
			ingredients:{
				...state.ingredients,
				[action.ingredientName]:state.ingredients[action.ingredientName] - 1
			},
			totalPrice:state.totalPrice - INGREDIENTS_PRICES[action.ingredientName],
			building:true


		};
		case actionTypes.SET_INGREDIENTS:
		return{
			...state,
			ingredients:{
			   salad:action.ingredients.salad,
			   bacon:action.ingredients.bacon,
			   cheese:action.ingredients.cheese,
			   meat:action.ingredients.meat
			},
			totalPrice: 20,
			error:false,
			building:false
		};
		case actionTypes.FETCH_INGREDIENTS_FAILED:
		return{
			...state,
			error:true
		}
		default:
		return state
	}
	

};
export default reducer;
