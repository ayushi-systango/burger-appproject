import axios from 'axios';

const instance = axios.create({
   baseURL: 'https://burger-builder-c4f06.firebaseio.com/'
   // 'https://react-my-burger-16e27.firebaseio.com/'
  

});

export default instance