import React, { Component } from 'react';
import Auxx from '../../hoc/Auxx';
import Burger from '../../Components/Burger/Burger';
import BuildControles from '../../Components/Burger/BuildControles/BuildControles';
import Modal from '../../Components/UI/Modal/Modal'
import OrderSummary from '../../Components/Burger/OrderSummary/OrderSummary';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../Components/UI/Spinner/Spinner';
import {connect} from 'react-redux';
import axios from '../../axios-order';
import * as actions from '../../store/actions/index';

import * as burgerBuilderActions from '../../store/actions/burgerBuilder';
import { purchaseInit } from '../../store/actions/order'; 
import { setAuthRedirectPath } from "../../store/actions/auth";


class BurgerBuilder extends Component {
    state = {
        purchasing: false
    }
    componentDidMount() {
        console.log(this.props);
        if(this.props.building && this.props.isAuthenticated)
        {
            this.props.history.push('/CheckOut');

        }
        else
        {
        this.props.onInitIngredients();
    }
       
    }

   
    updatedPurchaseState(ingredients) {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);
        return sum > 0;
    }

   
    purchaseHandler = () => {
        if(this.props.isAuthenticated)
        {
             this.setState({ purchasing: true });

        }
        else{
            this.props.onSetAuthRedirectPath('/CheckOut');
            this.props.history.push('/auth');
        }
    }
    purchaseCancelHandler = () => {
        this.setState({ purchasing: false });
    }

    purchaseContinueHandler = () => {
        this.props.onInitPurchase();
        this.props.history.push('/CheckOut');
    }
    render() {
        const disabledInfoLess = {
            ...this.props.ings

        };
        for (let key in disabledInfoLess) {
            disabledInfoLess[key] = disabledInfoLess[key] <= 0;
        }

        const disabledInfoMore = {
            ...this.props.ings

        };
        for (let key in disabledInfoMore) {
            disabledInfoMore[key] = disabledInfoMore[key] >= 2;
        }

        let orderSummary = null;
        let burger = this.props.error ? <p>INGREDIENTS Can't be loaded </p> : <Spinner />;

        if (this.props.ings) {
            burger = (
                <Auxx>
                    <Burger ingredients={this.props.ings} />
                    <BuildControles
                        ingredientAdded={this.props.onIngredientAdded}
                        ingredientRemoved={this.props.onIngredientRemoved}
                        purchasable={this.updatedPurchaseState(this.props.ings)}
                        disabledless={disabledInfoLess}
                        disabledmore={disabledInfoMore}
                        ordered={this.purchaseHandler}
                        isAuth={this.props.isAuthenticated}
                        price={this.props.price} />
                </Auxx>);

            orderSummary = <OrderSummary 
                ingredients={this.props.ings}
                price={this.props.price}
                purchaseCancelled={this.purchaseCancelHandler}
                purchaseContinue={this.purchaseContinueHandler}></OrderSummary>
        }
        return (
                <Auxx>
                    <Modal show={this.state.purchasing} modalClose={this.purchaseCancelHandler}>
                        {orderSummary}
                    </Modal>
                    {burger}
                </Auxx>
        );
    }
}
const mapStateToProps=state =>
{
    return{
        ings:state.burgerBuilder.ingredients,
        price:state.burgerBuilder.totalPrice,
        error:state.burgerBuilder.error,
        isAuthenticated:state.auth.token !== null,
        building:state.burgerBuilder.building
    };
}
const mapDispatchToProps = dispatch=>
{
    return{
        onIngredientAdded:(ingName) => dispatch(actions.addIngredient(ingName)),
        onIngredientRemoved:(ingName) => dispatch(actions.removeIngredient(ingName)),
        onInitIngredients:() => dispatch(actions.initIngredients()),
        onInitPurchase:() => dispatch(actions.purchaseInit()),
        onSetAuthRedirectPath:(path) => dispatch(actions.setAuthRedirectPath(path))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(BurgerBuilder , axios));