import React ,{Component} from 'react';
import CheckOutSummary from '../../Components/Order/CheckOutSummary/CheckOutSummary';
import{Route,Router , Redirect} from 'react-router-dom';
import history from '../../utils/history';
import ContactData from '../CheckOut/ContactData/ContactData';
import {connect} from 'react-redux';
import * as actions from '../../store/actions/index';
import './CheckOut.css';

class CheckOut extends Component
{
    
    checkoutCancelledHandler=()=>
    {
        this.props.history.replace('CheckOut/burgerBuilder');

    }
    checkoutContinueHandler=()=>
    {
        
        this.props.history.replace('/CheckOut/contact-data');
        return(
            <div className={'hidden'}>
            <CheckOut />
            </div>)


    }
    render()
    {
        let summary =<Redirect to="/"/>
        if(this.props.ings){
            const purchasedRedirect =this.props.purchased ? <Redirect to="/" /> : null;
            summary=(
                <div>
                {purchasedRedirect}
                <CheckOutSummary
                ingredients={this.props.ings}
                checkoutCancelled={this.checkoutCancelledHandler}
                checkoutContinue={this.checkoutContinueHandler}/>
                <Router history={history}>
                <Route exact path={this.props.match.path + "/contact-data"} 
                component={ContactData}/>

                </Router>
                </div>
                
                );

        }
        return summary;
    }

}
const mapStateToProps=state =>
{
    return{
        ings:state.burgerBuilder.ingredients,
        purchased:state.order.purchased

    }
};

export default connect(mapStateToProps)(CheckOut);