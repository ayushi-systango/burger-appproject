import React, { Component } from 'react';
import {connect} from 'react-redux';
import Layout from './Components/Layout/Layout';
import BurgerBuilder from './Contanirs/BurgerBuilder/BurgerBuilder';
import CheckOut from './Contanirs/CheckOut/CheckOut';
import Orders from './Contanirs/Orders/Orders';
import Auth from './Contanirs/Auth/Auth';
import Logout from './Contanirs/Auth/Logout/Logout';
import { Router, Route , Redirect} from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import history from './utils/history';
import LLayout from './hoc/Layout/LLayout';
import * as actions from './store/actions/index';

class App extends Component {
  componentDidMount(){
    this.props.onTryAutoSignup();
  }
 
  render() {
    let routes = (
      <Router history={history}>
        <Route path="/auth" component={Auth} />
        <Route path="/" exact component={BurgerBuilder} />
        <Redirect to="/" />
      </Router>
    );

    if (this.props.isAuthenticated) {
      routes = (
      <Router history={history}>
      <Route path="/checkout" component={CheckOut} />
          <Route path="/orders" component={Orders} />
          <Route path="/logout" component={Logout} />
          <Route path="/auth" component={Auth} />
          <Route path="/" exact component={BurgerBuilder} />
           <Redirect to="/" />
      </Router>
      );
    }
    return (
      <div>
        <LLayout>
          {routes}
        </LLayout>
        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

const mapDispatchToProps = dispatch =>{
  return{
    onTryAutoSignup:()=> dispatch(actions.authCheckState())
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(App);
