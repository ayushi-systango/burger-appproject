import React from 'react';
import BuildControl from './BuildControl/BuildControl';
import './BuildControles.css'

const controls = [
  { label: 'Salad(10)', type: 'salad' },
  { label: 'Bacon(40)', type: 'bacon' },
  { label: 'Cheese(30)', type: 'cheese' },
  { label: 'Meat(40)', type: 'meat' }
];

const buildControles = props => (
  <div className={'BuildControls'}>
  <p> <strong>Burger Slice Upto 2</strong></p>
    <p>Current Price:<strong>{props.price}</strong></p>
   
    {controls.map(ctrl => (
      <BuildControl
        key={ctrl.label}
        label={ctrl.label}
       added={()=>props.ingredientAdded(ctrl.type)}
       removed={()=>props.ingredientRemoved(ctrl.type)}
       disabledless={props.disabledless[ctrl.type]}
        disabledmore={props.disabledmore[ctrl.type]}
      />
    ))}
    <button 
    className={'OrderButton'}
    disabled={!props.purchasable}
    onClick={props.ordered}>{props.isAuth ? 'ORDER NOW' : 'SIGN UP To ORDER'}</button>
   
  </div>
);

export default buildControles;