import React from 'react';
import Auxx from '../../../hoc/Auxx'
import Button from '../../UI/Button/Button'
const OrderSummary=(props) => {
  const { purchaseCancelled, purchaseContinue } = props;
    const ingredientSummary = Object.keys(props.ingredients)
    .map(igKey => {
        return (
          <li key={igKey}>
            <span style={{ textTransform: 'capitalize' }}>{igKey}</span>:{' '}
            {props.ingredients[igKey]}
          </li>
        );
    });
    return(
        <Auxx>
            <h3>Your Order</h3>
            <p>A delicious burger with the following ingredients:</p>
            <ul>
                {ingredientSummary}
            </ul>
            <p>Total Price:{props.price}</p>
            <p>Continue to Checkout?</p>
            <Button btnType='Danger' clicked = {purchaseCancelled}>Cancel</Button>
            <Button btnType='Success' clicked={purchaseContinue}>Continue</Button>
      </Auxx>
    );

};
export default OrderSummary;