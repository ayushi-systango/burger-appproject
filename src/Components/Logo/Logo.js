import React from 'react';
import burgerlogo from '../../assets/images/burger-logo.png';
import './Logo.css';

const Logo = (props) => (
    <div className={'Logo'}style={{height:props.height}}>
        <img src={burgerlogo}  alt="MyBurger"  width="50px" height="50px"/>
    </div>
);

export default Logo;