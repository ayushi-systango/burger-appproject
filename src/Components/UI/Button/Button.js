import React from 'react';
import './Button.css';
const Button=(props)=>
(
    <button 
    disabled={props.disabled}
    className={props.btnType==='Success'? 'Button Success': 'Button Danger'}
    //className={'Button',[props.btnType].join(' ')}
    onClick={props.clicked}>{props.children}</button>
);
export default Button;