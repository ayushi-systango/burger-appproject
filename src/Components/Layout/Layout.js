import React from 'react';
import  './Layout.css';
import Auxx from '../../hoc/Auxx';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

const Layout=(props)=>(
    <Auxx>
    <div>
        <Toolbar/>
        <SideDrawer/> 
        </div>
    <main className={'C'}>
        {props.children}
    </main>
    </Auxx>
);
export default Layout;